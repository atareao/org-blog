URL=$(grep '#+LINK:' blog.org)

echo '<?xml version="1.0" encoding="UTF-8" ?>' > rss.xml
echo '<rss version="2.0">' >> rss.xml
echo ' ' >> rss.xml
echo '<channel>' >> rss.xml
echo ' ' >> rss.xml

# Buscar título del blog
grep -A 2 "#+TITLE:" blog.org >> rss.xml
sed -i "s/#+TITLE:/<title>/g" rss.xml
sed -i "s/#+LINK:/<link>/g" rss.xml 
sed -i "s/#+DESCRIPTION:/<description>/g" rss.xml

echo ' ' >> rss.xml
echo '<item>' >> rss.xml

# Buscar en blog.org los artículos
grep -A 2 ":TITLE:" blog.org >> rss.xml

# Renombrar :TITLE: , :EXPORT_FILE_NAME: , ...
sed -i "s/:TITLE:/<title>/g" rss.xml 
sed -i "s/:EXPORT_FILE_NAME:/<link>$URL/g" rss.xml
sed -i "s/:DESCRIPTION:/<description>/g" rss.xml
sed -i "s/\--/<\\\item><item>/g" rss.xml

echo '</item>' >> rss.xml
echo ' ' >> rss.xml

echo '</channel>' >> rss.xml
echo '</rss>' >> rss.xml

